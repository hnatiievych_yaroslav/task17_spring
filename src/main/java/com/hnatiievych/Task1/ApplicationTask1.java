package com.hnatiievych.Task1;

import com.hnatiievych.Task1.config.BeanConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class ApplicationTask1 {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(ApplicationTask1.class);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
        logger.info("context create " + context.getBeanDefinitionCount() + " bean");
        logger.info(Arrays.toString(context.getBeanDefinitionNames()));
        logger.info(context.getBeanDefinition("beanB"));
    }
}
