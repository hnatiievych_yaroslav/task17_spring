package com.hnatiievych.Task1.config;

import com.hnatiievych.Task1.model.*;
import org.springframework.context.annotation.*;

@Configuration
@Import(BeanConfigProperties.class)
public class BeanConfig {

    @Bean("beanA")
    public BeanA getBeanA1(BeanB beanB, BeanC beanC){
        return new BeanA(beanB.getName(), beanC.getValue());
    }
    @Bean ("beanA")
    public BeanA getBeanA2(BeanB beanB, BeanD beanD){
        return new BeanA(beanB.getName(), beanD.getValue());
    }
    @Bean ("beanA")
    public BeanA getBeanA3(BeanC beanC, BeanD beanD){
        return new BeanA(beanC.getName(), beanD.getValue());
    }

    @Bean ("beanE")
    @Lazy
    public BeanE getBeanE1(BeanA getBeanA1){
        return new BeanE(getBeanA1.getName(), getBeanA1.getValue());
    }
    @Bean ("beanE")
    @Lazy
    public BeanE getBeanE2(BeanA getBeanA2){
        return new BeanE(getBeanA2.getName(), getBeanA2.getValue());
    }
    @Bean ("beanE")
    @Lazy
    public BeanE getBeanE3(BeanA getBeanA3){
        return new BeanE(getBeanA3.getName(), getBeanA3.getValue());
    }
    @Bean
    public static MyBeanFactory getFactoryBean(){
        return new MyBeanFactory();
    }
    @Bean
    public MyBeanPostProcessor getMyBeanPostProcessor(){
        return new MyBeanPostProcessor();
    }
}