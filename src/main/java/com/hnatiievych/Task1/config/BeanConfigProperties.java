package com.hnatiievych.Task1.config;

import com.hnatiievych.Task1.model.BeanB;
import com.hnatiievych.Task1.model.BeanC;
import com.hnatiievych.Task1.model.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("my.properties")
public class BeanConfigProperties {
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;

    @Bean(value = "beanB",
    initMethod = "customInitMethod" ,
    destroyMethod = "customDestroyMethod")
    public BeanB getBeanB(){
        return new BeanB(name,value);
    }

    @Bean(value = "beanC",
            initMethod = "customInitMethod" ,
            destroyMethod = "customDestroyMethod")
    @DependsOn(value = {"beanD","beanB"})
    public BeanC getBeanC(){
        return new BeanC(name, value);
    }

    @Bean(value = "beanD",
            initMethod = "customInitMethod" ,
            destroyMethod = "customDestroyMethod")
    public BeanD getBeanD(){
        return new BeanD(name,value);
    }
}
