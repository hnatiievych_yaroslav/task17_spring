package com.hnatiievych.Task1.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    String name;
    int value;

    public BeanA() {
        System.out.println("empty constructer BeanA");
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
        System.out.println("2 param constructor BeanA");
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean validate() {
        System.out.println("validate BeanA");
        if(this.name!=null|this.value>0) {
            return true;
        }else return false;
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy BeanA");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("after property set BeanA");
    }
}
