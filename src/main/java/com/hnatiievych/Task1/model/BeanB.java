package com.hnatiievych.Task1.model;

import org.springframework.beans.factory.annotation.Value;

public class BeanB implements BeanValidator {
    @Value("${beanB.name}")
    String name;
    @Value("${beanB.value}")
    int value;

    public BeanB (){
        System.out.println("empty constructor BeanB");
    }

    public BeanB(String name, int value) {
        System.out.println("2 param costructor BeanB");
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public boolean validate() {
        System.out.println("validate BeanB");
        if(this.name!=null|this.value>0) {
            return true;
        }else return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    void customInitMethod (){
        System.out.println("custom init method BeanB");
    }
    void customDestroyMethod(){
        System.out.println("custom destroy method BeanB");
    }
    void initFactoryMethod(){
        System.out.println("custom init factory method");
    }
}
