package com.hnatiievych.Task1.model;

import org.springframework.beans.factory.annotation.Value;

public class BeanC implements BeanValidator {
    @Value("${beanC.name}")
    String name;
    @Value("${beanC.value}")
    int value;

public BeanC (){
    System.out.println("empty constructor BeanC");
}

    public BeanC(String name, int value) {
        System.out.println("2 param constructor BeanC");
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public boolean validate() {

    System.out.println("validate BeanC");
        if(this.name!=null|this.value>0) {
            return true;
        }else return false;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    void customInitMethod (){
        System.out.println("custom init method BeanC");
    }
    void customDestroyMethod(){
        System.out.println("custom destroy method BeanC");
    }
}
