package com.hnatiievych.Task1.model;

import org.springframework.beans.factory.annotation.Value;

public class BeanD implements BeanValidator {
    @Value("${beanD.name}")
    String name;
    @Value("${beanD.value}")
    int value;


    public BeanD() {
        System.out.println("empty constructor BeanD");
    }

    public BeanD(String name, int value) {
        System.out.println("2 param constructor BeanD");
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean validate() {
        System.out.println("validate BeanD");
        if (this.name != null | this.value > 0) {
            return true;
        } else return false;
    }


    void customInitMethod() {
        System.out.println("custom init method BeanD");
    }

    void customDestroyMethod() {
        System.out.println("custom destroy method BeanD");
    }
}
