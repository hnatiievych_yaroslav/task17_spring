package com.hnatiievych.Task1.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    String name;
    int value;


    public BeanE(String name, int value) {
        System.out.println("2 param constructor BeanE");
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public boolean validate() {

        System.out.println("validate BeanE");
        if(this.name!=null|this.value>0) {
            return true;
        }else return false;
    }

    @PostConstruct
    void customInitMethod (){
        System.out.println("custom init method BeanE");
    }
    @PreDestroy
    void customDestroyMethod(){
        System.out.println("custom destroy method BeanE");
    }
}

