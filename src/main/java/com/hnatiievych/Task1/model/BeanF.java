package com.hnatiievych.Task1.model;

public class BeanF implements BeanValidator {
    String name;
    int value;


    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public boolean validate() {
        System.out.println("validate BeanF");
        if(this.name!=null|this.value>0) {
            return true;
        }else return false;
    }
}
