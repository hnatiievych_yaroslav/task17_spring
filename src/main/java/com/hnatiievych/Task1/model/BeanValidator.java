package com.hnatiievych.Task1.model;

public interface BeanValidator {
    boolean validate();
}
